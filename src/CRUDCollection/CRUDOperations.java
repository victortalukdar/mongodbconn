package CRUDCollection;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;
import resources.PropertiesObject;

import java.io.File;
import java.util.Properties;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.*;



public class CRUDOperations {
    Document obj;

    public String getCollectionName()
    {
        String collectionName = null;

        try {
            String filelocation = "src/resources/MongoConnection.properties";
            String jdbcPropertiesFileName = new File(filelocation).getAbsolutePath();
            PropertiesObject propObject= new PropertiesObject();
            Properties prop = propObject.getProperties(jdbcPropertiesFileName);
            collectionName = prop.get("mongo.collectionName").toString();

        }
        catch (Exception e)
        {
            System.out.println("Cannot get the Mongo Connection Properties File !!");
        }

        return collectionName;
    }

    public String getInsertCollectionName()
    {
        String collectionName = null;

        try {
            String filelocation = "src/resources/MongoConnection.properties";
            String jdbcPropertiesFileName = new File(filelocation).getAbsolutePath();
            PropertiesObject propObject= new PropertiesObject();
            Properties prop = propObject.getProperties(jdbcPropertiesFileName);
            collectionName = prop.get("mongo.InsertCollectionName").toString();

        }
        catch (Exception e)
        {
            System.out.println("Cannot get the Mongo Connection Properties File !!");
        }

        return collectionName;
    }

    public void getAllDocuments(MongoDatabase mongoDatabase) {

        MongoCollection mongoCollection;

        String collectionName = this.getCollectionName();

        mongoCollection = this.getMongoCollection(mongoDatabase,collectionName);

       FindIterable<Document> docs = mongoCollection.find(); //SELECT * FROM sample;

        for (Document doc : docs) {

            System.out.println("JSON"+doc.toJson());
        }

    }

    public void getFirstDocument (MongoDatabase mongoDatabase) {

        MongoCollection mongoCollection;

        String collectionName = this.getCollectionName();

        mongoCollection = this.getMongoCollection(mongoDatabase,collectionName);

        Document obj = (Document) mongoCollection.find().first();

        System.out.println(obj.toJson());

    }

    public void getSpecificDocumentByID (MongoDatabase mongoDatabase) {

        MongoCollection mongoCollection;
        String collectionName = this.getCollectionName();

        mongoCollection = this.getMongoCollection(mongoDatabase,collectionName);

        Document obj = (Document) mongoCollection.find(new Document("_id", "10006546")).first();

        System.out.println(obj.toJson());
    }

    public MongoCollection getMongoCollection(MongoDatabase mongoDatabase, String collectionName){

        MongoCollection mongoCollection;

        mongoCollection = mongoDatabase.getCollection(collectionName);
        return mongoCollection;
    }


    public void getDocumentsByQuery (MongoDatabase mongoDatabase, String Query_Key, String Query_Value) {

        MongoCollection mongoCollection;
        mongoCollection = this.getMongoCollection(mongoDatabase,"listingsAndReviews");

        FindIterable<Document> docs = mongoCollection.find(new Document(Query_Key, Query_Value));

        for (Document doc : docs) {
            System.out.println("JSON"+doc.toJson());
        }
    }

    public void updateDocument (MongoDatabase mongoDatabase, String Query_Key, String Query_Value, String Update_Key, String Update_Value) {

        MongoCollection mongoCollection;

        String collectionName = this.getCollectionName();
        mongoCollection = this.getMongoCollection(mongoDatabase,collectionName);

        Bson filter;

        filter = eq("_id", "10006546");
        Bson updateOperation = set("name", "Ribeira Charming Duplex Duplex");

        obj = (Document) mongoCollection.findOneAndUpdate(filter,updateOperation);
    }

    public void getDocumentsByComplexORQuery (MongoDatabase mongoDatabase) {

        MongoCollection mongoCollection;

        String collectionName = this.getCollectionName();

        FindIterable<Document> docs = mongoDatabase.getCollection(collectionName).find(or
                (
                        eq("minimum_nights","2"),
                        eq("maximum_nights", "30"),
                        eq("_id", "10006546")
                )
        );

        for (Document doc : docs) {
            System.out.println("JSON"+doc.toJson());
        }
    }

    public void getDocumentsByComplexANDQuery (MongoDatabase mongoDatabase) {

        String collectionName = this.getCollectionName();

        FindIterable<Document> docs = mongoDatabase.getCollection(collectionName).find(and
                (
                        eq("minimum_nights","2"),
                        eq("maximum_nights", "30"),
                        eq("_id", "10006546")
                )
        );

        for (Document doc : docs) {
            System.out.println("JSON"+doc.toJson());
        }
    }

    public void createDocument(MongoDatabase mongoDatabase)
    {

       String collectionName = this.getInsertCollectionName();
        Document doc = new Document("studentName", "Victor Talukdar")
                .append("studentID", "213161")
                .append("language", "English")
                .append("ethnicity", "Indian")
                .append("citizenship","Indian");
        mongoDatabase.getCollection(collectionName).insertOne(doc);

    }

    }
